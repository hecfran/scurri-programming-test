######################################################################################################################################
# Scurry task 3 programming test
# Description:
# Write a library that supports validating and formatting postcodes for the UK. 
# The details of which postcodes are valid and which are the parts they consist of can be found at 
# https://en.wikipedia.org/wiki/Postcodes_in_the_United_Kingdom#Formatting 
# <https://workable.com/nr?l=https%3A%2F%2Fen.wikipedia.org%2Fwiki%2FPostcodes_in_the_United_Kingdom%23Formatting>. 
# The API that this library provides is your choice.
# 
# Candidate:
# Hector Franco
# April 2021
# 
###########################################################################################################################################
 
import re
from ukpostcodeutils import validation
#### 
# The exercise was implemented four times and each implementation can be called as:
# 1. reCheckUkCode(code)
# 2. re2CheckUkCode(code)
# 3. is_valid_postcode(code)
# 4. checkUkCode(code)








## auxiliar functions

def cleanString(string:str)->str:
    # tides the input post code
    s = string.rstrip().lstrip().upper().replace("  "," ").replace("\t"," ")
    return s

def exceptionalCode(code:str) -> str:
    # return true for 3 special post codes
    return (code in ['GIR 0AA','SA99','W1N 4DJ'])
        
    
#regular expression:([Gg][Ii][Rr] 0[Aa]{2})|((([A-Za-z][0-9]{1,2})|(([A-Za-z][A-Ha-hJ-Yj-y][0-9]{1,2})|(([A-Za-z][0-9][A-Za-z])|([A-Za-z][A-Ha-hJ-Yj-y][0-9][A-Za-z]?))))\s?[0-9][A-Za-z]{2})
# found at https://stackoverflow.com/questions/164979/regex-for-matching-uk-postcodes
# note the regular expression is also present in the wikipedia page https://en.wikipedia.org/wiki/Postcodes_in_the_United_Kingdom#Formatting
# ^(([A-Z]{1,2}[0-9][A-Z0-9]?|ASCN|STHL|TDCU|BBND|[BFS]IQQ|PCRN|TKCA) ?[0-9][A-Z]{2}|BFPO ?[0-9]{1,4}|(KY[0-9]|MSR|VG|AI)[ -]?[0-9]{4}|[A-Z]{2} ?[0-9]{2}|GE ?CX|GIR ?0A{2}|SAN ?TA1)$

def reCheckUkCode(code : str) -> bool:
    # returns true if the post code is valid for the UK
    assert isinstance(code, str), "the code needs to be a string"
    code = cleanString(code)
    if (exceptionalCode(code)):
        return True

    regularExp = r"([Gg][Ii][Rr] 0[Aa]{2})|((([A-Za-z][0-9]{1,2})|(([A-Za-z][A-Ha-hJ-Yj-y][0-9]{1,2})|(([A-Za-z][0-9][A-Za-z])|([A-Za-z][A-Ha-hJ-Yj-y][0-9][A-Za-z]?))))\s?[0-9][A-Za-z]{2})"
    a = re.match(regularExp, code)
    if a:
        return True
    else:
        return False




def re2CheckUkCode(code:str) -> bool:
    # second implementation using regular expressions, this time extracting parts  of the post code. 
    code = cleanString(code)
    if (exceptionalCode(code)):
        return True
    try:
        regex = "^(?:(?P<a1>[Gg][Ii][Rr])(?P<d1>) (?P<s1>0)(?P<u1>[Aa]{2}))|(?:(?:(?:(?P<a2>[A-Za-z])(?P<d2>[0-9]{1,2}))|(?:(?:(?P<a3>[A-Za-z][A-Ha-hJ-Yj-y])(?P<d3>[0-9]{1,2}))|(?:(?:(?P<a4>[A-Za-z])(?P<d4>[0-9][A-Za-z]))|(?:(?P<a5>[A-Za-z][A-Ha-hJ-Yj-y])(?P<d5>[0-9]?[A-Za-z]))))) (?P<s2>[0-9])(?P<u2>[A-Za-z]{2}))$"
        PARTS = re.search(regex, code).groups()
        p = [i for i in list(PARTS) if i] # removes Nones
        Area = p[0]
        District =p[1]
        Sector = p[2]
        Unit = p[3]
        if Area+District+" "+Sector+Unit != code:
            return False
        return checkParts(Area, District, Sector, Unit)
    except:
        return False
    
    return True



def is_valid_postcode(code: str) -> bool :
    # calls uk-postcode library
    # https://pypi.org/project/uk-postcode-utils/#files

    code = cleanString(code)
    if (exceptionalCode(code)):
        return True
    code = code.replace(" ","") # this library does not accept spaces inside the code.
    return validation.is_valid_postcode(code)


def checkUkCode(code : str) -> bool:
    code = cleanString(code)
    if (exceptionalCode(code)):
        return True
    
    
    def splitOutwardCode(code:str) -> (str,str):
        #assert len(code)>=2
        secondLetter = code[1]
        if secondLetter.isdigit():
            return code[0],code[1:]
        else:
            return code[0:2],code[2:]    

    def splitInwardCode(code:str) -> (str,str):
            return code[0],code[1:]            
                

    # returns true if the post code is valid for the UK
    assert isinstance(code, str), "the code needs to be a string"
    v = code.split(" ")
    if len(v)!=2:
        return False
    OutwardCode = v[0]
    if (not OutwardCode.isalnum()):
        return False

    InwardCode = v[1]
    if (not InwardCode.isalnum()):
        return False
    #return False



    # split outward code:
    Area,District = splitOutwardCode(OutwardCode)
    if  not District[0].isdigit():
        return False
    Sector,Unit = splitInwardCode(InwardCode)

    return checkParts(Area, District, Sector, Unit)


def getDistricNumber(District):
    r = ""
    for x  in District:
        if x.isdigit():
            r= r+x
    if r=="":
        return 0
    return int(r)

def checkParts(Area, District, Sector, Unit):
    # DisctrictLetters = District[1:]
    #   District[0]
    OutwardCode= Area+District
    # checking notes:

    #As all formats end with 9AA, the first part of a postcode can easily be extracted by ignoring the last three characters.
    if len(Unit)>2:
        return False
    
    #Areas with only single-digit districts: BR, FY, HA, HD, HG, HR, HS, HX, JE, LD, SM, SR, WC, WN, ZE (although WC is always subdivided by a further letter, e.g. WC1A)
    if Area in ['BR', 'FY', 'HA', 'HD', 'HG', 'HR', 'HS', 'HX', 'JE', 'LD', 'SM', 'SR',  'WN', 'ZE']:# 'WC'
        if not len(District)==1:            
            return False
    #Areas with only double-digit districts: AB, LL, SO
    if Area in ['AB', 'LL', 'SO']:
        if not len(District)==2:            
            return False

    ## VALID "BL3 3LQ"
    ###Areas with a district '0' (zero): BL, BS, CM, CR, FY, HA, PR, SL, SS (BS is the only area to have both a district 0 and a district 10)
    #if Area in ['BL', 'BS', 'CM', 'CR', 'FY', 'HA', 'PR', 'SL', 'SS']:
    #    if not DistrictNumber == '0':
    #        return False
    #if Area == 'BS':
    #    if not District in ['0','10']:
    #        return False


    #The following central London single-digit districts have been further divided by inserting a letter after the digit and before the space: 
    # EC1–EC4 (but not EC50), SW1, W1, WC1, WC2 and parts of E1 (E1W), N1 (N1C and N1P), NW1 (NW1W) and SE1 (SE1P).

    #EC1–EC4 (but not EC50),
    print(OutwardCode[0:2])
    if OutwardCode[0:2]=="EC":
        if OutwardCode[3].isdigit():#EC50
            return False
        n = getDistricNumber(OutwardCode[2])
        if (n<1 or n>4):
            return False
    #SW1-SW20## https://www.milesfaster.co.uk/london-postcodes-list.htm#:~:text=The%20Centre%20of%20London%20is,the%20'City%20of%20London'.
    if OutwardCode[0:2]=="SW":
        n = getDistricNumber(OutwardCode[2:])
        if n>20 or n<1:
            return False
    
    if OutwardCode[0:2]=="SE":
        n = getDistricNumber(OutwardCode[2:])
        if n>28 or n<1:
            return False

        
    #W1, WC1, WC2
    if OutwardCode[0]==1:
        if OutwardCode[1].isdigit():
            if OutwardCode[1]!=1:
                return False
        if OutwardCode[1]=='C':
            n = getDistricNumber(OutwardCode[2])
        if (n<1 or n>2):
            return False            
    
    #E1 (E1W),???????????
    #  N1 (N1C and N1P), ????????
    #  NW1 (NW1W) ???????????
    #  and SE1 (SE1P). ?????


    
    

    

    #The letters Q, V and X are not used in the first position.
    if Area[0] in ['Q','V','X']:
        return False
        
    #The letters I, J and Z are not used in the second position.
    if len(Area)>1:
        if Area[1] in ['I','J','Z']:
            return False
    

    #The only letters to appear in the third position are A, B, C, D, E, F, G, H, J, K, P, S, T, U and W when the structure starts with A9A.
    if len(OutwardCode)==3:
        if OutwardCode[1].isdigit():
            if (not OutwardCode[2] in ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'J', 'K', 'P', 'S', 'T', 'U','W']) and (not OutwardCode[2].isdigit()): # should i add N?
                return False



    #The only letters to appear in the fourth position are A, B, E, H, M, N, P, R, V, W, X and Y when the structure starts with AA9A.
    if len(OutwardCode)==4:
        if OutwardCode[2].isdigit():
            if not OutwardCode[3] in ['A', 'B', 'E', 'H', 'M', 'N', 'P', 'R', 'V', 'W','X','Y'] and (not OutwardCode[3].isdigit()):
                return False


    #The final two letters do not use C, I, K, M, O or V, so as not to resemble digits or each other when hand-written.
    if Unit[0] in ['C','I','K','M','O','V']:
        return False
    if Unit[1] in ['C','I','K','M','O','V']:
        return False
    return True # if it doesn't break any rule, accept it. 