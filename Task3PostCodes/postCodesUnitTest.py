import unittest
import postCodes


#https://www.ukpostcode.co.uk/random.htm

class TestCodes(unittest.TestCase):
    
    def test_ukCodes(self):
        filepath = './testCodes/UK_codes.txt'
        with open(filepath) as fp:
            line = fp.readline()
            cnt = 1 # counter to find the line being process
            while line:
                t1 = postCodes.reCheckUkCode(line)
                t2 = postCodes.re2CheckUkCode(line)
                t3 = postCodes.checkUkCode(line)
                t4 = postCodes.is_valid_postcode(line)
                #print('line {} : "{}"  code'.format(cnt, line.strip())) # for debugging only

                assert(t1==t2)
                assert(t1==t3)
                assert(t1==t4)

                if t1:                    
                    print('valid, line {} : "{}" is a UK postal code'.format(cnt, line.strip()))
                else:
                    print('invalid, line {} : "{}" is not a UK postal code'.format(cnt, line.strip()))
                    assert False, 'Not a uk postal code'
                line = fp.readline()
                cnt += 1

    def test_nonukCodes(self):
        filepath = './testCodes/non_UK_codes.txt'
        with open(filepath) as fp:
            line = fp.readline()
            cnt = 1
            while line:
                t1 = postCodes.reCheckUkCode(line)
                t2 = postCodes.re2CheckUkCode(line)
                t3 = postCodes.checkUkCode(line)
                t4 = postCodes.is_valid_postcode(line)

                assert(t1==t2)
                assert(t1==t3)   
                assert(t1==t4)
             
                if t1:
                    print('valid, line {} : "{}" is a UK postal code'.format(cnt, line.strip()))
                    assert False, 'false aceptance of a non a uk postal code'
                else:
                    print('invalid, line {} : "{}" is not a UK postal code'.format(cnt, line.strip()))
                    
                line = fp.readline()
                cnt += 1
                  
    def test_ukspecialCodes(self):
        filepath = './testCodes/special.txt'
        with open(filepath) as fp:
            line = fp.readline()
            cnt = 1
            while line:
                #print('line {} : "{}" is code'.format(cnt, line.strip()))
            
                t1 = postCodes.reCheckUkCode(line)
                t2 = postCodes.re2CheckUkCode(line)
                t3 = postCodes.checkUkCode(line)
                t4 = postCodes.is_valid_postcode(line)
                print(line)

                assert(t1==t2)
                assert(t1==t3)
                assert(t1==t4)
                if t1:
                    print('s. valid, line {} : "{}" is a UK postal code'.format(cnt, line.strip()))
                else:
                    print('s. invalid, line {} : "{}" is not a UK postal code'.format(cnt, line.strip()))
                    assert False, 'Not a uk postal code'
                line = fp.readline()
                cnt += 1
            
unittest.main(verbosity=2, exit=False) #run the tests
