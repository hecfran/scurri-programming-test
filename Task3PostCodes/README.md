# Scurri programming test

Task description: 
Write a library that supports validating and formatting postcodes for the UK. The details of which postcodes are valid and which are the parts they consist of can be found at https://en.wikipedia.org/wiki/Postcodes_in_the_United_Kingdom#Formatting <https://workable.com/nr?l=https%3A%2F%2Fen.wikipedia.org%2Fwiki%2FPostcodes_in_the_United_Kingdom%23Formatting>. The API that this library provides is your choice.


 
## Four implementations:
There are four implementations of how to check the validity of a UK post code:
1. The simples is based on a public library ukpostcodeutils https://pypi.org/project/uk-postcode-utils/, and just calls this library, <br> API is def is_valid_postcode(code): <br> 
   While this is the most straight  forward implementation, other implementations are present to showcase the author's programming skills. 
2. The second uses a regular expression found at https://stackoverflow.com/questions/164979/regex-for-matching-uk-postcodes, <br> API: def reCheckUkCode(code : str) -> bool:
3. Also uses a regular expression found at https://gist.github.com/joshlk/92d0e25d847a490b86369ec1c31a39c5 <br> API: re2CheckUkCode(code:str) -> bool:
4. Uses splits and individual checks to split the post code into atomic parts. <br> API: def checkUkCode(code : str) -> bool:


## Wikipedia restriction notes:
The implementations of the third and fourth version share code for verifying the consistency of the parts according to the notes found in wikipedia:
* As all formats end with 9AA, the first part of a postcode can easily be extracted by ignoring the last three characters.
* Areas with only single-digit districts: BR, FY, HA, HD, HG, HR, HS, HX, JE, LD, SM, SR, WC, WN, ZE (although WC is always subdivided by a further letter, e.g. WC1A)
* Areas with only double-digit districts: AB, LL, SO
* Areas with a district '0' (zero): BL, BS, CM, CR, FY, HA, PR, SL, SS (BS is the only area to have both a district 0 and a district 10)
* The following central London single-digit districts have been further divided by inserting a letter after the digit and before the space: EC1–EC4 (but not EC50), SW1, W1, WC1, WC2 and parts of E1 (E1W), N1 (N1C and N1P), NW1 (NW1W) and SE1 (SE1P).
* The letters Q, V and X are not used in the first position.
* The letters I, J and Z are not used in the second position.
* The only letters to appear in the third position are A, B, C, D, E, F, G, H, J, K, P, S, T, U and W when the structure starts with A9A.
* The only letters to appear in the fourth position are A, B, E, H, M, N, P, R, V, W, X and Y when the structure starts with AA9A.
* The final two letters do not use C, I, K, M, O or V, so as not to resemble digits or each other when hand-written.
* Postcode sectors are one of ten digits: 0 to 9, with 0 only used once 9 has been used in a post town, save for Croydon and Newport (see above).

## Unit tests
The unit tests uses three files:
* Genuine random uk post-codes
* Special uk post codes (eg. SA99	Driver and Vehicle Licensing Agency)
* Invalid  post-codes.

In the test all four implementations are required to make the same output. 

## Notes:

### invalid codes:
codes like "AB1 0NY" to  my understanding  are invalid because the area AB requires a double-digit 
district and in this case, the district is just "1".
this case was removed from the unit test because uk-postcode-utils/ library accepts it as 
valid, but the notes from Wikipedia suggest it is invalid. 
This could be due to a problem of interpretation of those restrictions.



