####
# Scurry task 2 programming test
# description:
# Write a program that prints the numbers from 1 to 100. 
# But for multiples of three print “Three” instead of the number and for the multiples of five print “Five”. 
# For numbers that are multiples of both three and five print “ThreeFive”. 
# 
# candidate:
# Hector Franco
# April 2021
# 
 


def getStringNumber(number: int) -> str:
    # Transfrom a number into the string required to compleat this task, numbers has to be greater than zero. 
    assert number>0, "the number has to be greater than zero"
    numbMod5 = number % 5==0
    numbMod3 = number % 3==0
    if numbMod5 and numbMod3:
        return "ThreeFive"
    if numbMod3:
        return "Three"
    if numbMod5:
        return "Five"
    return str(number)


for x in range(100):
    print(getStringNumber(x+1))
    