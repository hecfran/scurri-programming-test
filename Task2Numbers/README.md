# Scurri programming test

Task desciption: 
Write a program that prints the numbers from 1 to 100. But for multiples of three print “Three” instead of the number and for the multiples of five print “Five”. For numbers that are multiples of both three and five print “ThreeFive”. 

How to run it:

python scurryNumbers.py