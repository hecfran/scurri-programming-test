import unittest
import scurriNumbers 

class TestNumbers(unittest.TestCase):
    def test_one(self):
        s = scurriNumbers.getStringNumber(1)
        print(s)
        assert s=="1"
    def test_three(self):
        s = scurriNumbers.getStringNumber(3)
        print(s)
        assert s=="Three"        
    def test_five(self):
        s = scurriNumbers.getStringNumber(5)
        print(s)
        assert s=="Five"                
    def test_fivethree(self):
        s = scurriNumbers.getStringNumber(15)
        print(s)
        assert s=="ThreeFive"                
        
unittest.main(verbosity=2, exit=False) #run the tests
