# Scurri programming test



## Tasks: 
* Tell us about one thing you are proud of in your career. It could be a difficult technical problem you had to solve in your work or a personal project. There is no need to go into details; a short paragraph explaining the problem and why you are proud of it would be fine. 
* Write a program that prints the numbers from 1 to 100. But for multiples of three print “Three” instead of the number and for the multiples of five print “Five”. For numbers that are multiples of both three and five print “ThreeFive”. 
* Write a library that supports validating and formatting postcodes for the UK. The details of which postcodes are valid and which are the parts they consist of can be found at https://en.wikipedia.org/wiki/Postcodes_in_the_United_Kingdom#Formatting <https://workable.com/nr?l=https%3A%2F%2Fen.wikipedia.org%2Fwiki%2FPostcodes_in_the_United_Kingdom%23Formatting>. The API that this library provides is your choice.

## Candiate:
Hector Franco <br>
francoph@tcd.ie
