"""
check folder checks if path to the curren folder 
Hector Franco
4-nov-2020
"""

from glob import glob
from os import path
import sys
import os


ignoreList = ["__pycache__",".ipynb_checkpoints","/."]



def checkFolder(folder, maxDepth=15):
    """ 
    check folder checks if path to the curren folder 
    takes a given folder and maxim depth search to avoid loops on linux filesystem.    
    """ 
    
    errors = []
    #check early stop conditions
    if (maxDepth==0):
        return [];
    for exception in ignoreList:
        if (exception in folder):
            return []
                    
    print("checking: " + folder)
    #check readmyfile
    readme = path.exists(folder+"README.md")   
    if readme==False:
        errors.append(folder)
    #check sub-folders
    b = glob(folder+"*/")
    for f in b:     
        errors += checkFolder(f, maxDepth-1)
    return errors
      
def findMissingReadmeFiles(folder):
    missing = checkFolder(folder)
    if len(missing)==0:
        print("all sub-folders have a README.md file  :-)")
    else:
        print("the following folders do not have a README.md file:")
        for x in missing:
            print(x)
        raise NameError("missing README.md files")

folder = "./"      
if len(sys.argv)>=2:
    folder = sys.argv[1]

if os.path.isdir(folder)==False:
    raise NameError("the given folder (" + folder + ") does not exists")
      
findMissingReadmeFiles(folder)

